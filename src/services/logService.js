function init() {
  /*
  Raven.config("https://d89ac219385245fdb85e8f7e1862a459@sentry.io/1502215",
  {    
    release: '1-0-0',
    environment: 'development-test',
  }).install();
  */
}

function log(error) {
  // Raven.captureException(error);
  console.error(error);
}

export default {
  init,
  log
};

