import http from './httpService';

const apiEndpoint = '/movies';

export async function getMovies() {
  const { data: movies } = await http.get(apiEndpoint);
  return [...movies];
}

export async function deleteMovie(id) {
  const { data: movie } = await http.delete(`${apiEndpoint}/${id}`);
  return movie;
}

export async function getMovie(movieId) {
  const { data: movie } = await http.get(`${apiEndpoint}/${movieId}`);
  return movie;
}

export async function saveMovie(movie) {
  let result;
  if (movie._id) {
    const id = movie._id;
    delete movie._id;
    result = await http.put(`${apiEndpoint}/${id}`, movie);
  } else {
    result = await http.post(apiEndpoint, movie);
  }
  return result;
}
