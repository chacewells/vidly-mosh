import React, { Component } from 'react';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import MoviesTable from './moviesTable';
import Pagination from './common/pagination';
import ListGroup from './common/listGroup';
import SearchBox from './common/searchBox';
import { getMovies, deleteMovie } from '../services/movieService';
import { getGenres } from '../services/genreService';
import { paginate } from '../utils/paginate';
import _ from 'lodash';

export default class Movies extends Component {
  state = {
    movies: [],
    genres: [],
    currentPage: 1,
    pageSize: 4,
    selectedGenre: null,
    sortColumn: { path: 'title', order: 'asc' },
    searchQuery: '',
  };

  async componentDidMount() {
    const data = await getGenres();
    const genres = [{ _id: '', name: 'All Genres' }, ...data];

    const movies = await getMovies();
    this.setState({ movies, genres });
  }

  handleDelete = async movie => {
    // optimistic update
    const originalMovies = [...this.state.movies];
    const movies = originalMovies.filter(m => m._id !== movie._id);
    this.setState({ movies });
    try {
      await deleteMovie(movie._id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        toast.error('This movie has already been deleted.');

      this.setState({ movies: originalMovies });
    }
  };

  handleLike = movie => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index] = { ...movies[index] };
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };

  handlePageChange = currentPage => {
    this.setState({ currentPage });
  };

  handleGenreSelect = genre => {
    this.setState({ selectedGenre: genre, currentPage: 1, searchQuery: '', });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  handleSearch = searchQuery => {
    this.setState({ searchQuery, currentPage: 1, selectedGenre: null, });
  };

  getLikeClass(movie) {
    let classes = 'fa fa-heart';
    if (!movie.like)
      classes += '-o';
    return classes;
  }

  getPagedData() {
    const {
      pageSize,
      currentPage,
      selectedGenre,
      movies: allMovies,
      sortColumn,
      searchQuery,
    } = this.state;

    let filtered = allMovies;
    if (searchQuery) {
      const lowerFilter = searchQuery.toLowerCase();
      filtered = allMovies.filter(m => m.title.toLowerCase().startsWith(lowerFilter));
    } else if (selectedGenre && selectedGenre._id) {
      filtered = allMovies.filter(m => m.genre._id === selectedGenre._id)
    }

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    const movies = paginate(sorted, currentPage , pageSize);

    return { totalCount: filtered.length, data: movies };
  }

  render() {
    const { length: count } = this.state.movies;
    const { pageSize, currentPage, searchQuery } = this.state;
    const { user } = this.props;

    const { totalCount, data: movies } = this.getPagedData();

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-2">
            <ListGroup
              items={this.state.genres}
              clearValue="All Genres"
              selectedItem={this.state.selectedGenre}
              onItemSelect={this.handleGenreSelect}
            />
          </div>
          <div className="col">
            {user && (<Link 
              to="/movies/new" 
              className="btn btn-primary"
              style={{ marginBottom: 20 }}
            >
              New Movie
            </Link>)}
            <SearchBox
              value={searchQuery}
              onChange={this.handleSearch}
            />
            <p>Showing {totalCount} movies in the database.</p>
            <MoviesTable
              movies={movies}
              onLike={this.handleLike}
              onDelete={this.handleDelete}
              onSort={this.handleSort}
              sortColumn={this.state.sortColumn}
            />
            <Pagination
              pageSize={pageSize}
              onPageChange={this.handlePageChange}
              itemsCount={totalCount}
              currentPage={currentPage}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}
