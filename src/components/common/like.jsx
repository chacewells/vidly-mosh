import React from 'react';

const style = { cursor: 'pointer' };

const Like = ({ liked, onClick }) => {
  let classes = 'fa fa-heart';
  if (!liked) classes += '-o';
  return <i className={classes} onClick={onClick} style={style}></i>;
};

export default Like;
